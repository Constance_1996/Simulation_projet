function SIGMA=covariance(zone) %zone(:,1) = vB 1 ere colonne etc ...
M=[cov(zone(:,1),zone(:,1)) cov(zone(:,1),zone(:,2)) cov(zone(:,1),zone(:,3)) cov(zone(:,1),zone(:,4)) cov(zone(:,1),zone(:,5)) cov(zone(:,1),zone(:,6));
    cov(zone(:,2),zone(:,1)) cov(zone(:,2),zone(:,2)) cov(zone(:,2),zone(:,3)) cov(zone(:,2),zone(:,4)) cov(zone(:,2),zone(:,5)) cov(zone(:,2),zone(:,6));
    cov(zone(:,3),zone(:,1)) cov(zone(:,3),zone(:,2)) cov(zone(:,3),zone(:,3)) cov(zone(:,3),zone(:,4)) cov(zone(:,3),zone(:,5)) cov(zone(:,3),zone(:,6));
    cov(zone(:,4),zone(:,1)) cov(zone(:,4),zone(:,2)) cov(zone(:,4),zone(:,3)) cov(zone(:,4),zone(:,4)) cov(zone(:,4),zone(:,5)) cov(zone(:,4),zone(:,6));
    cov(zone(:,5),zone(:,1)) cov(zone(:,5),zone(:,2)) cov(zone(:,5),zone(:,3)) cov(zone(:,5),zone(:,4)) cov(zone(:,5),zone(:,5)) cov(zone(:,5),zone(:,6));
    cov(zone(:,6),zone(:,1)) cov(zone(:,6),zone(:,2)) cov(zone(:,6),zone(:,3)) cov(zone(:,6),zone(:,4)) cov(zone(:,6),zone(:,5)) cov(zone(:,6),zone(:,6))];

SIGMA=[M(1,2) M(1,4) M(1,6) M(1,8) M(1,10) M(1,12); 
    M(3,2) M(3,4) M(3,6) M(3,8) M(3,10) M(3,12);
    M(5,2) M(5,4) M(5,6) M(5,8) M(5,10) M(5,12);
    M(7,2) M(7,4) M(7,6) M(7,8) M(7,10) M(7,12);
    M(9,2) M(9,4) M(9,6) M(9,8) M(9,10) M(9,12);
    M(11,2) M(11,4) M(11,6) M(11,8) M(11,10) M(11,12)];
end