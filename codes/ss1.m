function tes=ss1(CpT,CpD,CbT,CbD,k1,k2,k3,k4,kb,vb)



%PLASMA DATA 0 / Cp
%TOTAL DATA 0 / Cb

%-----Patient 467 zone0------------




%p = polyfit(Cp(:,1),Cp(:,2),8)
%x1 = linspace(0,5500);
%y1 = polyval(p,x1);
%figure
%plot(Cp(:,1),Cp(:,2),'o')
%hold on
%plot(x1,y1)
%hold off
dt=0:0.1:5000/60;
YCP=interp1(CpT/60,CpD,dt);
YCB=interp1(CbT/60,CbD,dt);

n = 2;
SYS=sys(k1,k2,k3,k4,kb,vb,n);
[Y,T,X] = lsim(SYS,[YCP' YCB'],dt);

plot(T,Y)
hold on
plot(T,X,'r--')
hold off
end

function y=sys(k1,k2,k3,k4,kb,vb,n)

A=[-(k2(n)+k3(n)) k4(n) 0;k3(n) -k4(n) 0;0 0 0];
B=[k1(n) 0 ;0 0 ;kb(n) 0 ];
C=[1-vb(n) 1-vb(n) vb(n)];
D=[0 vb(n)];

y=ss(A,B,C,D);

end
function [new_T, new_D] = reg_lin(old_T, old_D, delta_t)
    % cette fonction retourne les vecteur new_T et new_D contenant les nouvelles valeurs
    
    f = cell(1,length(old_D)-1); % cellule o� sont stock�es toutes les "sous-fonctions" qui approximent la courbe
    new_T = old_T(1):delta_t:old_T(end); % linspace sur tout l'intervalle de temps avec le delta_t choisi
    new_D = zeros(length(new_T)); % vecteur o� sont stock�es toutes les nouvelles valeurs

    for i = 1:length(old_D)-1 % pour chaque intervalle entre 2 valeurs de data
        a = (old_D(i+1)-old_D(i))/(old_T(i+1)-old_T(i));
        b = old_D(i) - a*old_T(i);
        f{i} = @(x) a*x+b; % fonction affine reliant les 2 valeurs (points) sur le graphe
    end

    c = 1; % compteur pour savoir sur quel intervalle de temps on se trouve pour utiliser la bonne sous-fonction
    for j = 1:length(new_T)
        if new_T(j) > old_T(c+1)
            c = c+1;
        end
        new_D(j) = f{c}(new_T(j)); % remplissage de new_D
    end

  %  figure(1)
   % plot(old_T,old_D);
    %figure(2)
    %plot(new_T,new_D);
end

