function exp=testvtexp(CpT,CpD,CbT,CbD,R,n)

K1=zeros(100,1);
k2=zeros(100,1);
k3=zeros(100,1);
k4=zeros(100,1);
Kb=zeros(100,1);
vB=zeros(100,1);
exp=zeros(100,1);
for i=1:100
   K1(i,1)=R(i,2);
   k2(i,1)=R(i,3);
   k3(i,1)=R(i,4);
   k4(i,1)=R(i,5);
   Kb(i,1)=R(i,6);
   vB(i,1)=R(i,1);


%PLASMA DATA 0 / Cp
%TOTAL DATA 0 / Cb





dt=0:0.1:5000/60;
YCP=interp1(CpT/60,CpD,dt);
YCB=interp1(CbT/60,CbD,dt);

SYS=sys(K1(i,1),k2(i,1),k3(i,1),k4(i,1),Kb(i,1),vB(i,1),n);
[Y,T,X] = lsim(SYS,[YCP' YCB'],dt);

% X(:,1) : Cnd
% X(:,2) : Cs
% X(:,3) : Cvasc
% Y : Cmeasured

plot(T,Y)
hold on
plot(T,X,'r--')
hold off

%--------------Intégrale Part1--------------------
time_PET=[0.166666667
0.416666667
0.583333333
0.75
0.916666667
1.083333333
1.25
1.416666667
1.583333333
1.75
1.916666667
2.166666667
2.5
2.833333333
3.5
4.5
6
8
10
12
14
17.5
22.5
27.5
32.5
37.5
42.5
47.5
52.5
57.5
65
75
85];

%intCtissue=zeros(20,1);
%Val=zeros(20,1); 

%On prend que les premieres valeurs de Y pour Ctissue
%xinte=[10 25 35 45 55 65 75 85 95 105 115 130 150 170 210 270 360 480 600 720 ];
%yinte=[Y(10) Y(25) Y(35) Y(45) Y(55) Y(65) Y(75) Y(85) Y(95) Y(105) Y(115) Y(130) Y(150) Y(170) Y(210) Y(270) Y(360) Y(480) Y(600) Y(720)];
intCtissue=cumtrapz(T,Y);
intCT = interp1(T,intCtissue,time_PET,'linear','extrap');
CT=interp1(T,Y,time_PET,'linear','extrap');
intCP=cumtrapz(CpT/60,CpD);
intCP = interp1(CpT/60,intCP,time_PET,'linear','extrap');
Ordonnees=intCT./CT;
abscisse=intCP./CT;
a1=Ordonnees(end-8:end);
a2=abscisse(end-8:end);
p=polyfit(a2,a1,1);
exp(i,1)=p(1,1);
plot(abscisse,Ordonnees,'-o')

hold on 
plot(abscisse,polyval(p,abscisse))
legend('(integr CT)/CT)','VT*(integr CP)/CT')
title('logan plot')



%for i=1:length(xinte)

 %   Val(i)=intCtissue(i)/yinte(i);
%end
%Val;



%---------------Integrale Part2-----------------
%intCP=zeros(14,1);
%CpT14=zeros(14,1);
%CpD14=zeros(14,1);
%for i=1:14 %on prend que les 14 premieres valeurs de Cp
 %   CpT14(i)=CpT(i);
  %  CpD14(i)=CpD(i);
%end
    
%intCP=cumtrapz(CpT14,CpD14);
%Val2=zeros(14,1);

%for i=1:length(CpT14)
 %   Val2(i)=intCP(i)/CpD14(i);
%end
%a1=(Val(20)-Val(15))/(xinte(20)-xinte(15));
%a2=(Val2(14)-Val2(11))/(CpT14(14)-CpT14(11));
%a=a1/a2
%b=0;
%Val3=a*Val2 +b;
%plot(xinte,Val,'-o')
%hold on
%plot(CpT14,Val2,'-o')
%plot(CpT14,Val3)
%plot(Val2,Val)
end
end
function y=sys(k1,k2,k3,k4,kb,vb,n)

A=[-(k2(1)+k3(1)) k4(1) 0;k3(1) -k4(1) 0;0 0 0];
B=[k1(1) 0 ;0 0 ;kb(1) 0 ];
C=[1-vb(1) 1-vb(1) vb(1)];
D=[0 vb(1)];

y=ss(A,B,C,D);

end

