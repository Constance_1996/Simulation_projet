function y=erreur(vtt,vte)
s=0;
m=zeros(100,1);
for i=1:100
s=s+abs(((vtt(i,1)-vte(i,1))/vtt(i,1)))*100;
m(i,1)=abs(((vtt(i,1)-vte(i,1))/vtt(i,1)))*100;
end
y=s/100;
ecart=std(m)
end