function tes=alea100(CpT,CpD,CbT,CbD,R)



%PLASMA DATA 0 / Cp
%TOTAL DATA 0 / Cb

%-----Patient 467 zone0------------




%p = polyfit(Cp(:,1),Cp(:,2),8)
%x1 = linspace(0,5500);
%y1 = polyval(p,x1);
%figure
%plot(Cp(:,1),Cp(:,2),'o')
%hold on
%plot(x1,y1)
%hold off
dt=0:0.1:5000/60;
YCP=interp1(CpT/60,CpD,dt);
YCB=interp1(CbT/60,CbD,dt);

for i=1:100
SYS=sys(R(i,2),R(i,3),R(i,4),R(i,5),R(i,6),R(i,1));
[Y,T,X] = lsim(SYS,[YCP' YCB'],dt);

plot(T,Y)
hold on
end
end

function y=sys(k1,k2,k3,k4,kb,vb)

A=[-(k2+k3) k4 0;k3 -k4 0;0 0 0];
B=[k1 0 ;0 0 ;kb 0 ];
C=[1-vb 1-vb vb];
D=[0 vb];

y=ss(A,B,C,D);

end