function R=graphlg(MU,SIGMA)
new_pat=mvnrnd(MU, SIGMA, 100);
R=exp(new_pat);
end